package com.example.hello.hellorest;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hello.hellorest.api.Greeting;
import com.example.hello.hellorest.api.GreetingEndpointApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity {
    private GreetingEndpointApi greetingService;
    private Button personalizedGreeingBtn;
    private Button nonPersonalizedGreeingBtn;
    private Button formatBtn;
    private TextView nonPersonalizedId;
    private TextView nonPersonalizedResponse;
    private TextView personalizedId;
    private TextView personalizedResponse;
    private EditText imie;
    private EditText format;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initService();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindUI();
        bindListeners();
    }

    private void bindListeners() {
           //TODO
    }

    private void bindUI(){
        personalizedGreeingBtn = (Button) findViewById(R.id.personalizedGreetingBtn);
        nonPersonalizedGreeingBtn = (Button) findViewById(R.id.greetingBtn);
        nonPersonalizedId = (TextView) findViewById(R.id.nonPersonalizedId);
        nonPersonalizedResponse = (TextView) findViewById(R.id.nonPersonalizedResponse);
        personalizedId = (TextView) findViewById(R.id.personalizedId);
        personalizedResponse = (TextView) findViewById(R.id.personalizedResponse);
        imie = (EditText) findViewById(R.id.imie);
        format = (EditText) findViewById(R.id.format);
        formatBtn = (Button) findViewById(R.id.fmtBtn);
    }

    private void initService(){
       //TODO
    }
}
